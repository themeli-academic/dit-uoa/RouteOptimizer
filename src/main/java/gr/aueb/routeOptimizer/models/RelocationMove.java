package gr.aueb.routeOptimizer.models;

import java.util.Objects;

// Tracks a customer swap between different routes. Used in Tabu Search
// The four integer fields are the indexes in the origin and target lists
// and the moveCost keeps the cost of moving a customer from the origin lists
// to the target
public class RelocationMove extends Move {

    private Integer originRoutePosition;
    private Integer originClientPosition;
    private Integer targetRoutePosition;
    private Integer targetClientPosition;
    private Double moveCost;

    public RelocationMove() {
        super(-1, -1);
    }

    // Constructor to create a new RelocationMove object and instanciate the fields
    // With super() calls the constructor of the super class Move to instanciate also the fields origin and target (see Move class)
    public RelocationMove(Integer originRoutePosition, Integer originClientPosition, Integer targetRoutePosition,
                          Integer targetClientPosition, Double moveCost) {
        super(originRoutePosition, targetRoutePosition);
        this.originRoutePosition = originRoutePosition;
        this.originClientPosition = originClientPosition;
        this.targetRoutePosition = targetRoutePosition;
        this.targetClientPosition = targetClientPosition;
        this.moveCost = moveCost;
    }

    public Integer getOriginRoutePosition() {
        return originRoutePosition;
    }

    public void setOriginRoutePosition(Integer originRoutePosition) {
        this.originRoutePosition = originRoutePosition;
    }

    public Integer getOriginClientPosition() {
        return originClientPosition;
    }

    public void setOriginClientPosition(Integer originClientPosition) {
        this.originClientPosition = originClientPosition;
    }

    public Integer getTargetRoutePosition() {
        return targetRoutePosition;
    }

    public void setTargetRoutePosition(Integer targetRoutePosition) {
        this.targetRoutePosition = targetRoutePosition;
    }

    public Integer getTargetClientPosition() {
        return targetClientPosition;
    }

    public void setTargetClientPosition(Integer targetClientPosition) {
        this.targetClientPosition = targetClientPosition;
    }

    public Double getMoveCost() {
        return moveCost;
    }

    public void setMoveCost(Double moveCost) {
        this.moveCost = moveCost;
    }

    @Override
    public String toString() {
        return "RelocationMove{" +
                "originRoutePosition=" + originRoutePosition +
                ", originClientPosition=" + originClientPosition +
                ", targetRoutePosition=" + targetRoutePosition +
                ", targetClientPosition=" + targetClientPosition +
                ", moveCost=" + moveCost +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RelocationMove that = (RelocationMove) o;
        return Objects.equals(originRoutePosition, that.originRoutePosition) &&
                Objects.equals(originClientPosition, that.originClientPosition) &&
                Objects.equals(targetRoutePosition, that.targetRoutePosition) &&
                Objects.equals(targetClientPosition, that.targetClientPosition) &&
                Objects.equals(moveCost, that.moveCost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(originRoutePosition, originClientPosition, targetRoutePosition, targetClientPosition, moveCost);
    }
}
