package gr.aueb.routeOptimizer.executors;

import gr.aueb.routeOptimizer.models.Solution;
import gr.aueb.routeOptimizer.models.TruckRoute;
import gr.aueb.routeOptimizer.models.VisitLocation;
import gr.aueb.routeOptimizer.utils.ResultsWriter;
import gr.aueb.routeOptimizer.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class LocalSearchExecutor {

    private List<List<Double>> distanceMatrix;

    public void localSearch(Solution currentSolution, List<List<Double>> distanceMatrix) {
        System.out.println("============= Local Search Algorithm =================");
        this.distanceMatrix = distanceMatrix;
        // get the list of routes from the BPP solution
        List<TruckRoute> routes = currentSolution.getRoutes();
        // create an empty list of TruckRoute objects
        List<TruckRoute> newRoutes = new ArrayList<>();
        // counter to check how many times LocalSearch algorithm is executed
        int countRepetitions = 0;
        // this loop will stop only when the customer list in all routes will remain the same
        while(true) {
            // list with flags to check if a new solution is produced (list of customers is changed in any route)
            List<Boolean> routesChanged = new ArrayList<>();
            // iterate all routes and init the routesChanged list with false value in all items
            for(int i=0; i<routes.size();i++) {
                routesChanged.add(Boolean.FALSE);
            }
            // iterate the routes list again
            for (TruckRoute route : routes) {
                // find whether a better route exists (with reduced total cost)
                TruckRoute newRoute = findBestNeighbor(route);
                // if the order of clients to visit has changed, update the flag of the specific route to true
                if(!route.getClientsToVisit().equals(newRoute.getClientsToVisit())) {
                    routesChanged.set(routes.indexOf(route), Boolean.TRUE);
                } else {
                    // otherwise return the flag to false for this route
                    routesChanged.set(routes.indexOf(route), Boolean.FALSE);
                }
                // add the route in the new list of routes
                newRoutes.add(newRoute);
            }
            // if all flags are false break the while loop
            // we have found a local minimum
            if(!routesChanged.contains(Boolean.TRUE)) {
                System.out.println("Best solution already found!");
                break;
            }
            // update the counter of algorithm repetitions
            countRepetitions ++;
            System.out.println("Repetion " + countRepetitions + " - Result is: ");
            // print the routes of the new solution
            currentSolution.getRoutes().forEach(System.out::println);
            // write the solution in a file (in json format)
            ResultsWriter.writeResults(currentSolution, Utils.LOCAL_SEARCH_RESULTS_FILE + countRepetitions + ".json");
            // assign the newRoutes list to the routes list
            routes = newRoutes;
            // create a new empty list to execute the algorithm again
            newRoutes = new ArrayList<>();

        }
        // print the total cost of the local minimum solution
        System.out.println("Cost for LS for " + countRepetitions + " iterations: " + currentSolution.getTotalCost());
        System.out.println("============= Finished Local Search Algorithm =================");
    }

    /**
     * Get the best local search neighbor of the input route
     * @param route input
     * @return the best neighbor
     */
    private TruckRoute findBestNeighbor(TruckRoute route) {
        // create a new TruckRoute object with the same values in its fields
        TruckRoute bestRoute = Utils.cloneRoute(route);
        // iterate customers to the third from last customer
        for(int i=1; i<route.getClientsToVisit().size() - 2; i++) {
            // iterate customers to the second last customer
            for(int j = i+1; j<route.getClientsToVisit().size() - 1; j++) {
               // swap, get the new list of customers
                List<VisitLocation> candidate = swapLocations(route.getClientsToVisit(), i, j);
                // get cost of the new list of customers
                // clone the route object
                TruckRoute newRoute = Utils.cloneRoute(route);
                newRoute.setClientsToVisit(candidate);
                newRoute.setTotalDistance(Utils.calCostOfRoute(newRoute, distanceMatrix));
                // if the cost is reduced, update the best route with the current route object
                if(newRoute.getTotalDistance() < route.getTotalDistance()){
                    bestRoute = newRoute;
                    // update the total time needed to serve all the customers
                    Utils.updateTotalTime(bestRoute, distanceMatrix);
                }
            }
        }
        return bestRoute;
    }
    // create a new list of customers 
    //iterate the list of customers and swp i and j customers
    private List<VisitLocation> swapLocations(List<VisitLocation> baseline, int i, int j){
        List<VisitLocation> candidate = new ArrayList<>();
        for (int k=0; k< baseline.size(); ++k){
            if (k==i) candidate.add(baseline.get(j));
            else if(k==j) candidate.add(baseline.get(i));
            else candidate.add(baseline.get(k));
        }
        return candidate;
    }
}
