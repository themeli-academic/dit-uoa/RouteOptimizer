package gr.aueb.routeOptimizer.models;

// Superclass to RelocationMove and SwapMove
public class Move {
    protected Integer origin;
    protected Integer target;

    public Move(Integer origin, Integer target) {
        this.origin = origin;
        this.target = target;
    }

    public boolean isEqual(Move m){
        // identical moves
        // return m.origin.equals(origin) && m.target.equals(target);
        // inverse moves
        return m.origin.equals(target) && m.target.equals(origin);
    }


}
