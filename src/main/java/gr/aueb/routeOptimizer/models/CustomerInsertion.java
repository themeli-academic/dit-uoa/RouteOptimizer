package gr.aueb.routeOptimizer.models;

import java.io.Serializable;

// Keeps a new insertion of a customer
// visitLocation determines which customer will be inserted
// insertionRoute is the route in which customer will be inserted
// cost tracks the total cost of this insertion and 
// insertionPositionIndex determines the index of the clientsToVisit list of the route
// where customer will be inserted
public class CustomerInsertion implements Serializable {

    private VisitLocation visitLocation;
    private TruckRoute insertionRoute;
    private Double cost;
    private Integer insertionPositionIndex;

    public CustomerInsertion() {

    }

    public CustomerInsertion(VisitLocation visitLocation, TruckRoute insertionRoute, Double cost, Integer insertionPositionIndex) {
        this.visitLocation = visitLocation;
        this.insertionRoute = insertionRoute;
        this.cost = cost;
        this.insertionPositionIndex = insertionPositionIndex;
    }

    public VisitLocation getVisitLocation() {
        return visitLocation;
    }

    public void setVisitLocation(VisitLocation visitLocation) {
        this.visitLocation = visitLocation;
    }

    public TruckRoute getInsertionRoute() {
        return insertionRoute;
    }

    public void setInsertionRoute(TruckRoute insertionRoute) {
        this.insertionRoute = insertionRoute;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Integer getInsertionPositionIndex() {
        return insertionPositionIndex;
    }

    public void setInsertionPositionIndex(Integer insertionPositionIndex) {
        this.insertionPositionIndex = insertionPositionIndex;
    }

    @Override
    public String toString() {
        return "CustomerInsertion{" +
                "visitLocation=" + visitLocation +
                ", insertionRoute=" + insertionRoute +
                ", cost=" + cost +
                ", insertionPositionIndex=" + insertionPositionIndex +
                '}';
    }
}
