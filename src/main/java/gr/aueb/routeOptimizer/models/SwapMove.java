package gr.aueb.routeOptimizer.models;

import java.util.Objects;

// This class keeps a track of a Move for the TabuSearch
// Keeps the initial (first) indexes in the route and clients lists
// and the new indexes in the second route and client lists
public class SwapMove extends Move {

    private Integer firstRoutePosition;
    private Integer firstClientPosition;
    private Integer secondRoutePosition;
    private Integer secondClientPosition;
    private Double moveCost;

    public SwapMove() {
        super(-1, -1);

    }


    public boolean isEqual(Move m){
        // identical or inverse moves
        return (m.origin.equals(target) && m.target.equals(origin)) ||
               (m.origin.equals(origin) && m.target.equals(target));
    }

    // Constructor of SwapMove class to instanciate the fields of the class
    // super() is used so as to call the constructor of the Move class to instanciate origin and target fields
    public SwapMove(Integer firstRoutePosition, Integer firstClientPosition, Integer secondRoutePosition,
                    Integer secondClientPosition, Double moveCost) {
        super(firstRoutePosition, secondRoutePosition);
        this.firstRoutePosition = firstRoutePosition;
        this.firstClientPosition = firstClientPosition;
        this.secondRoutePosition = secondRoutePosition;
        this.secondClientPosition = secondClientPosition;
        this.moveCost = moveCost;
    }

    public Integer getFirstRoutePosition() {
        return firstRoutePosition;
    }

    public void setFirstRoutePosition(Integer firstRoutePosition) {
        this.firstRoutePosition = firstRoutePosition;
    }

    public Integer getFirstClientPosition() {
        return firstClientPosition;
    }

    public void setFirstClientPosition(Integer firstClientPosition) {
        this.firstClientPosition = firstClientPosition;
    }

    public Integer getSecondRoutePosition() {
        return secondRoutePosition;
    }

    public void setSecondRoutePosition(Integer secondRoutePosition) {
        this.secondRoutePosition = secondRoutePosition;
    }

    public Integer getSecondClientPosition() {
        return secondClientPosition;
    }

    public void setSecondClientPosition(Integer secondClientPosition) {
        this.secondClientPosition = secondClientPosition;
    }

    public Double getMoveCost() {
        return moveCost;
    }

    public void setMoveCost(Double moveCost) {
        this.moveCost = moveCost;
    }

    @Override
    public String toString() {
        return "SwapMove{" +
                "firstRoutePosition=" + firstRoutePosition +
                ", firstClientPosition=" + firstClientPosition +
                ", secondRoutePosition=" + secondRoutePosition +
                ", secondClientPosition=" + secondClientPosition +
                ", moveCost=" + moveCost +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SwapMove swapMove = (SwapMove) o;
        return Objects.equals(firstRoutePosition, swapMove.firstRoutePosition) &&
                Objects.equals(firstClientPosition, swapMove.firstClientPosition) &&
                Objects.equals(secondRoutePosition, swapMove.secondRoutePosition) &&
                Objects.equals(secondClientPosition, swapMove.secondClientPosition) &&
                Objects.equals(moveCost, swapMove.moveCost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstRoutePosition, firstClientPosition, secondRoutePosition, secondClientPosition, moveCost);
    }
}
