package gr.aueb.routeOptimizer;

import gr.aueb.routeOptimizer.executors.BppAlgorithmExecutor;
import gr.aueb.routeOptimizer.executors.InputDataExecutor;
import gr.aueb.routeOptimizer.executors.LocalSearchExecutor;
import gr.aueb.routeOptimizer.executors.TabuSearchExecutor;
import gr.aueb.routeOptimizer.models.Solution;
import gr.aueb.routeOptimizer.models.VisitLocation;

import java.util.List;

public class RouteOptimizer {

    // instanciate all executors
    private InputDataExecutor inputDataExecutor = new InputDataExecutor();
    private BppAlgorithmExecutor bppAlgorithm = new BppAlgorithmExecutor();
    private LocalSearchExecutor localSearchExecutor = new LocalSearchExecutor();
    private TabuSearchExecutor tabuSearchExecutor = new TabuSearchExecutor();

    // this is the starting point of the project
    // args is given when you execute the project (e.g. java RouteOptimizer 50)
    // if a number is given in the args, we keep this as the number of customers, otherwise the default value is 100
    public static void main(String[] args) {
        int numberOfCustomers = 100;
        if (args.length > 0) {
            numberOfCustomers = Integer.parseInt(args[0]);
            System.out.println("Got number of customers: ");
        }
        System.out.println("Setting number of customers to the default value of " + numberOfCustomers);
        // execute all the questions
        RouteOptimizer routeOptimizer = new RouteOptimizer();
        routeOptimizer.executeProject(numberOfCustomers);
    }

    private void executeProject(int numberOfCustomers) {
        // use InputDataExecutor to create the list of customers and the 2D distance array
        List<VisitLocation> visitLocations = inputDataExecutor.createAllNodesAndCustomerLists(numberOfCustomers);
        List<List<Double>> distanceMatrix = inputDataExecutor.createDistanceMatrix(visitLocations);
//        Utils.printDistanceMatrix(distanceMatrix);
        // produce a solution with BPP algorithm
        Solution currentSolution = bppAlgorithm.bppAlgorithm(visitLocations, distanceMatrix);
        // create a new solution object from the currentSolution 
        Solution copyCurrentSolution = new Solution();
        copyCurrentSolution.setTotalCost(currentSolution.getTotalCost());
        copyCurrentSolution.setRoutes(currentSolution.getRoutes());
        // execute LocalSearch algorithm
        localSearchExecutor.localSearch(copyCurrentSolution, distanceMatrix);
        // execute TabuSearch algorithm
        tabuSearchExecutor.tabuSearch(copyCurrentSolution, visitLocations, distanceMatrix);
    }

}
