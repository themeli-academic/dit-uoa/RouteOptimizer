package gr.aueb.routeOptimizer.models;

import java.io.Serializable;

// This class represents a customer. Each customer has an id, a boolean field
// which determines if the customer is already visited by a truck, the latitude and longitude
// (x and y fields), the service time (standard 15 minutes) and the number of products
public class VisitLocation implements Serializable {

    private Integer clientId;
    private Boolean visited;
    private Integer latitude;
    private Integer longitude;
    private Double serviceTime;
    private Integer products;

    public VisitLocation() {

    }

    public VisitLocation(Integer clientId, Boolean visited, Integer latitude,
                         Integer longitude, Double serviceTime, Integer products) {
        this.clientId = clientId;
        this.visited = visited;
        this.latitude = latitude;
        this.longitude = longitude;
        this.serviceTime = serviceTime;
        this.products = products;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Boolean getVisited() {
        return visited;
    }

    public void setVisited(Boolean visited) {
        this.visited = visited;
    }

    public Integer getLatitude() {
        return latitude;
    }

    public void setLatitude(Integer latitude) {
        this.latitude = latitude;
    }

    public Integer getLongitude() {
        return longitude;
    }

    public void setLongitude(Integer longitude) {
        this.longitude = longitude;
    }

    public Double getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(Double serviceTime) {
        this.serviceTime = serviceTime;
    }

    public Integer getProducts() {
        return products;
    }

    public void setProducts(Integer products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "VisitLocation{" +
                "clientId=" + clientId +
                ", visited=" + visited +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", serviceTime=" + serviceTime +
                ", products=" + products +
                '}';
    }
}
