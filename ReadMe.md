# Documentation

## Problem modeling

For the requirements of the project, java classes that model the problem are created. The model classes can be found
under the package gr.aueb.routeOptimizer.models. The java classes used are: VisitLocation, TruckRoute, Solution, CustomerInsertion, RelocationMove, SwapMove.

* VisitLocation class represents a customer order with fields the visitLocation id, the location (latitude and longitude), the service time to unload the ordered products and a boolean field to track if a customer is already visited.
* TruckRoute class represents a truck used to deliver products to the customers. In the class level, we used the static fields
capacity and velocity. Each Truck object is also characterized by the filled and the remaining space as well as a list
of customers to visit.
* Solution class contains the total cost of a solution (total distance for all the used trucks) and a list of the TruckRoute objects used.

The three following classes are used for Tabu Search (question 4).

* CustomerInsertion: customer to be inserted in a route. The fields used are: route, cost, visit location and the index in the route list to be inserted.
* RelocationMove: tracks a move of a customer to a different list of routes. Keeps the indexes of the origin and target route as well as the index of the customer in the origin and targe list.
* SwapMove: tracks a customer move between neighboring routes. Similary, keeps the necessary indexes.

As input to the problem, the provided code is used, parameterized to use the above models. In this way, we store a list
of visitLocations/orders, defining the visitLocation id, the latitude, the longitude, the ordered products and the service time.

## Baseline

As baseline, Bin Packing Problem(BPP) algorithm is implemented. In RouteOptimizer, the method bppAlgorithm (BppAlgorithmExecutor) takes as input the visitLocation's list and assigns to each visitLocation the best matching truck which will deliver the order.

As best matching truck, we define the truck that has the less remaining space (but not being full) and also the remaining space is enough in order to add the visitLocations' products. Additionally, it is checked whether the total trip time is less than 5 hours for each truck. The output of this algorithm is in the file RouteOptimizer/BppAlgorithmResults.json. Despite that distance between customer locations is not taken into account in this question, the total cost of each route (and of the solution) is calculated and extracted to the file, in order to be compared with the results of the next questions.

## Local Search

Question 3 (LocalSearch algorithm) is implemented in the class LocalSearchExecutor. The method gets as input the solution of the previous question, i.e. the list of the used routes, and for each route all the customers are swaped in order to find a better neighboring solution. The swapping is implemented as a double for loop over the locations. If a better candidate is found that has a reduced cost, the best cost and route are updated, with the best overall route being returned for every iteration and route. The process ends if no route is changed within an iteration, which signals that a local optimum has been encountered.

By executing the algorithm with 100 customers as input, the local minimum is reached after 3 iterations. The total cost of the final solution is 4865.65 while the total number of routes is 29. We considered that the space of each truck was optimized in the previous question and we changed the customers' positions only inside their route.

## Tabu Search

The Tabu search process is initialized with the Local Search best solution. Using the code supplied in the course, we loop for 10000 repetitions optimizin the solution via tabu search. At each iteration, either a swap or a relocation move is applied: the first swaps two random locations in a single route, while the second moves a location accross routes. Both operations are restricted to discard illegal or non meaningful moves and the best possible move is selected by exhaustive search with for loops. In addition, we mantain a tabu list to enhance the algorithm with heuristics to avoid local optima and get trapped in circular search. Specifically, we define a move via its source and destination route index. This is implemented by adding a superclass for each move type with the aforementioned fields and defining the tabu list as a list of these generic moves. A move is judged as tabu if it is a relocation move and a move with inverse origin and destination routes already exists in the list, or if it is a swap move and a move with same or inverse origin and target routes exists in the list.

The tabu list is implemented as a java LinkedList, and at each iteration an element is discarded in FIFO order. The move comparison is done by defining the isEqual method for the generic move that detects inverse moves. This is overriden in the swap move class, which check for identical and inverse moves.

The best cost produced by this algorithm is 2237.33 and the result (with the routes) can be found in RouteOptimizer/TabuSearchResults.json
