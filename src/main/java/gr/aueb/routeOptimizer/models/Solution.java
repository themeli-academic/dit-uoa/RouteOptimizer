package gr.aueb.routeOptimizer.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

// Represents the solution of an algorithm
// Each solution has a total cost field (the total distance for all the routes)
// and a list with the produced routes
public class Solution implements Serializable {

    private Double totalCost = 0.0;
    private List<TruckRoute> routes = new ArrayList<>();

    // Default constructor to create a new Solution object
    public Solution() {

    }

    // Constructor to create a new Solution object and instanciate the fields of the Solution object
    public Solution(Double totalCost, List<TruckRoute> routes) {
        this.totalCost = totalCost;
        this.routes = routes;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public List<TruckRoute> getRoutes() {
        return routes;
    }

    public void setRoutes(List<TruckRoute> routes) {
        this.routes = routes;
    }

    @Override
    public String toString() {
        return "Solution{" +
                "totalCost=" + totalCost +
                ", routes=" + routes +
                '}';
    }
}
